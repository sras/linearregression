module Main where

import Lib
import GradientDescent
import NormalEquation
import Data.Decimal
import Debug.Trace
import Data.Monoid
import Data.Vector as V
import Prelude  (snd, Bool(..), (<), (==), fst, show, pure, read, reads, fromIntegral, error, (-), (/), (*), (+), writeFile, fmap, (.), ($), (<$>), IO, FilePath, Int, String, Either(..), Float)
import qualified Prelude as P
import HMatrixStatic
import qualified Data.Text as T
import GHC.TypeNats
import qualified Numeric.LinearAlgebra as LA
import LogisticRegression
import qualified Data.Map as DM
import Control.Monad.Writer
import Control.Monad.Reader
import qualified Control.Exception as E
import Prelude
import DataGen
import Codec.Picture
import NeuralNet
import Network.Wai.EventSource
import Control.Concurrent.Chan
import Control.Concurrent
import Data.Binary.Builder
import Data.Text.Encoding as TE
import Data.ByteString.Base64.Lazy

import Servant ( QueryParam
               , PlainText
               , OctetStream
               , err400
               , ServantErr(..)
               , Post
               , Get
               , throwError
               , Raw
               , ServerT
               , ReqBody
               , JSON
               , Proxy(..)
               , type (:>)      -- Syntax for importing type operator
               , type (:<|>)
               , (:<|>)(..)
               , Accept(..)
               , MimeRender(..)
               )
import Servant.Server (Handler, Server, Application, serve)
import Servant.Utils.StaticFiles
import Network.Wai.Handler.Warp (run)
import Control.Monad.IO.Class (liftIO)
import Data.ByteString.Lazy.Char8 as C

data PNG

instance Accept PNG where
   contentType _ = "image/png"

instance MimeRender PNG ByteString  where
   mimeRender _ val = val

type ServantType = "meanNormalize" :> ReqBody '[JSON] T.Text :> Post '[JSON] T.Text
              :<|> "extractOutput" :> ReqBody '[JSON] [T.Text] :> Post '[JSON] [T.Text]
              :<|> "insertX0" :> ReqBody '[JSON] T.Text :> Post '[JSON] T.Text
              :<|> "linearRegression" :> ReqBody '[JSON] [T.Text] :> Post '[JSON] [T.Text]
              :<|> "addFeature" :> ReqBody '[JSON] [T.Text] :> Post '[JSON] T.Text
              :<|> "testHypothesis" :> ReqBody '[JSON] [T.Text] :> Post '[JSON] [T.Text]
              :<|> "logisticRegression" :> ReqBody '[JSON] [T.Text] :> Post '[JSON] [T.Text]
              :<|> "filter" :> ReqBody '[JSON] T.Text :> Post '[JSON] T.Text
              :<|> "trainNetwork" :> Raw
              :<|> Raw

showState :: (TVector a, P.Double) -> T.Text
showState (c, e) = T.pack $ show $ (e, LA.toList $ unr c)

getMatrix :: T.Text -> Handler (TMatrix a b)
getMatrix t = let
  vectorTs = loadDataFromText t (convertTraningItem <$> commaSeparated)
  in case tmatrixSafe vectorTs of
    Right x -> return x
    Left e -> throwError $ err400 { errReasonPhrase = e }

getVector :: T.Text -> Handler (TVector a)
getVector t = let
  vectorTs = loadDataFromText t (convertTraningItem <$> commaSeparated)
  in return (tvectorSafe $ V.head <$> vectorTs)

filterTrainingSet :: T.Text -> Handler T.Text
filterTrainingSet inp = do
  let
    vectorTs = loadDataFromText inp (convertTraningItem <$> commaSeparated)
    filtered = V.filter (\x -> V.last x == 9) vectorTs
  return $ showTMatrix $ tmatrix filtered

addFeatureHandler :: [T.Text] -> Handler T.Text
addFeatureHandler [inp, tcol, texp] = do
  TMatrix t <- getMatrix inp
  let
    rows = LA.rows t
    col = P.read $ T.unpack tcol
    exp = P.read $ T.unpack texp :: Int
    subMatrix = (LA.subMatrix (0, col) (rows, 1) t) P.^ exp
    result = TMatrix (t LA.||| subMatrix)
  return $ showTMatrix result

linearRegressionHandler :: [T.Text] -> Handler [T.Text]
linearRegressionHandler [inp, out, coeff, lr, tun, tualr] = do
  t <- getMatrix inp
  o <- getVector out
  let un = tun == "True"
  let ualr = tualr == "True"
  if P.not un then do
    c <- getVector coeff
    let learningRate = if ualr then 0.00003 else readDouble lr
    let ((error, theta), log) = runWriter $ runGradientDescent t o lrHypothesis lrCostFunction learningRate c 2000 un ualr
    return [T.pack $ show error, showVectorVertical $ showVector $ unr theta, T.unlines $ showState <$> log]
  else let
    (theta, error) = runNormalEquationOn t o in
    return [T.pack $ show error, showVectorVertical $ showVector $ unr theta]

linearRegressionNEHandler :: [T.Text] -> Handler [T.Text]
linearRegressionNEHandler [inp, out, coeff] = do
  t <- getMatrix inp
  o <- getVector out
  let (theta, error) = runNormalEquationOn t o
  return [T.pack $ show error, showVectorVertical $ showVector $ unr theta]

logisticRegressionHandler :: [T.Text] -> Handler [T.Text]
logisticRegressionHandler [inp, out, coeff, lr, tualr]  = do
  t <- getMatrix inp
  o <- getVector out
  c <- getVector coeff
  let ualr = tualr == "True"
  let learningRate = if ualr then 0.003 else readDouble lr
  let (error, theta, log) = runLogisticRegression c t learningRate 1000 o
  return [T.pack $ show error, showVectorVertical $ showVector $ unr theta, T.unlines $ showState <$> log]

meanNormaizeHandler :: T.Text -> Handler T.Text
meanNormaizeHandler inp = do
  t <- getMatrix inp
  let (d, md) = meanNormalize t in return $ showTMatrix d

insertX0Handler :: T.Text -> Handler T.Text
insertX0Handler inp = do
  t <- getMatrix inp
  return $ showTMatrix $ (getX0 (rows t)) ||| t

extractOutputHandler :: [T.Text] -> Handler [T.Text]
extractOutputHandler [tmatrix, tcol] = do
  TMatrix t <- getMatrix tmatrix
  let
    col = P.read $ T.unpack tcol :: Int
    columns = LA.toColumns t
    leftColumns = (P.take col columns)
    rightColumns = (P.drop (col + 1) columns)
    outputVector = columns P.!! col
  trainingSet <- case (P.length leftColumns, P.length rightColumns) of
    (0, 0) -> throwError $ err400
    (0, _) -> return $ LA.fromColumns $ rightColumns
    (_, 0) -> return $ LA.fromColumns $ leftColumns
    (_, _) -> return $ (LA.fromColumns leftColumns) LA.||| (LA.fromColumns rightColumns)
  return $ [showTMatrix $ TMatrix trainingSet, showVectorVertical $ showVector outputVector]

testHypothesisHandler :: [T.Text] -> Handler [T.Text]
testHypothesisHandler [tmatrix, theta, texpected, isLgr] = do
  t <- getMatrix tmatrix
  h <- getVector theta
  e <- getVector texpected
  let
    lgr = isLgr == "True"
    x = (getX0 (rows t)) ||| t
  if lgr then let
    pred = lgrHypo h x
    cost = lgrCostFunction pred e
    in return $ [showVectorVertical $ showVector $ unr pred, T.pack $ show cost]
  else let
      pred = lrHypothesis h x
      cost = lrCostFunction pred e
    in return $ [showVectorVertical $ showVector $ unr pred, T.pack $ show cost]

trainNetworkHandler :: ServerT Raw m
trainNetworkHandler = return trainApp
  where
  trainApp :: Application
  trainApp req resp = do
      chan <- newChan
      dg :: DataGenerator 6400 1000 <- getDataGenerator 1
      nn <- randomNetwork sigmoid pSigmoid 2 1 2 5 :: IO (NeuralNet 2 1 5 3)
      -- let 
      --   nn = getNetwork
      let
        ain = inputs dg
        aout = applyDivider divider $ ain
        is = toTaggedRows $ ain
        es = toTaggedRows aout
        pairs = (taggedZip is es)
      forkIO $ do
        Prelude.putStrLn "Forking"
        trainNetwork nn ain aout 0.03 0 10000 100 (writeChannel ain aout pairs dg chan)
        return ()
      (eventSourceAppChan $ chan) req resp
  writeChannel ain aout pairs dg chan (count, mnn) = do
    Prelude.putStrLn "Network recieved"
    se <- makeServerEvent
    if mod count 1 == 0 then do
      Prelude.putStrLn $ show $ currentCost
      writeChan chan se else return ()
    where
      currentCost = case mnn of
        Just (nn, cost) -> cost
        Nothing -> -1
      makeServerEvent :: IO ServerEvent
      makeServerEvent = case mnn of
        Nothing -> return $ CloseEvent
        Just (nn, cost) -> do
          let
            gin = grid dg
            gout = applyDivider (makeDivider nn) gin
          i <- getPlot2 nn gin gout ain aout $ scale dg
          case encodeDynamicPng i of
            Right o -> do
              return $ ServerEvent Nothing Nothing [(fromLazyByteString $ encode $ o )]
            Left err -> undefined

server :: Server ServantType
server = meanNormaizeHandler
    :<|> extractOutputHandler
    :<|> insertX0Handler
    :<|> linearRegressionHandler
    :<|> addFeatureHandler
    :<|> testHypothesisHandler
    :<|> logisticRegressionHandler
    :<|> filterTrainingSet
    :<|> trainNetworkHandler
    :<|> serveDirectoryWebApp "html"

app :: Application
app = serve (Proxy :: Proxy ServantType) server

main :: IO ()
main = run 8000 app
