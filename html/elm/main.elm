module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Debug exposing (log, crash)
import String exposing (toInt)
import Http as H
import Json.Encode as Encode
import Json.Decode as Decode
import Ports exposing (..)


type Route
    = Index
    | PreProcessing PreProcessingRoute
    | Learning LearningRoute
    | Test TestRoute
    | Plot


type TestRoute
    = Hypothesis


type PreProcessingRoute
    = ExtractOutput
    | MeanNormalize
    | Filter
    | InsertX0
    | AddFeature


type LearningRoute
    = LinearRegression
    | LogisticRegression


type alias Model =
    { route : Route
    , meanNormalizeInput : String
    , meanNormalizeOutput : String
    , extractOutputInput : String
    , extractOutputColumn : String
    , extractOutputTrainingSet : String
    , extractOutputOutputVector : String
    , insertX0Input : String
    , insertX0Output : String
    , lrtsUseNormalEq : Bool
    , lrtsInput : String
    , lrtsOInput : String
    , lrtsCInput : String
    , lrtsLrAuto : Bool
    , lrtsLr : String
    , lrError : String
    , lrCoeff : String
    , lrLog : String
    , addFeatureTSInput : String
    , addFeatureColumnInput : String
    , addFeatureExpInput : String
    , addFeatureOutput : String
    , testHypoInputM : String
    , testHypoInputTheta : String
    , testHypoInputExpected : String
    , testHypoIsLogistic : Bool
    , testHypoOutputActual : String
    , testHypoOutputError : String
    , lgrtsInput : String
    , lgrtsOInput : String
    , lgrtsCInput : String
    , lgrtsLrAuto : Bool
    , lgrtsLr : String
    , lgrCoeff : String
    , lgrError : String
    , lgrLog : String
    , filterInput : String
    , filterOutput : String
    }


type Msg
    = ChangeRoute Route
    | MeanNormalizeInput String
    | MeanNormalizeOutput (Result H.Error String)
    | SendMeanNormalizeRequest
    | ExtractOutputInput String
    | ExtractOutputOutput (Result H.Error (List String))
    | ExtractOutputColumn String
    | SendExtractOutput
    | SendInsertX0Request
    | InserX0Input String
    | InsertX0Output (Result H.Error String)
    | LRTSInput String
    | LRTSOInput String
    | LRTSCInput String
    | LRTSLrInput String
    | LRTSUseNormalEqToggle
    | LRTSUseAutoLRToggle
    | SendLinearRegressionRequest
    | LinearRegressionOutput (Result H.Error (List String))
    | AddFeatureOutput (Result H.Error String)
    | SendAddFeatureRequest
    | AddFeatureTSInput String
    | AddFeatureExpInput String
    | AddFeatureColumnInput String
    | SendTestHypothesisRequest
    | TestHypothesisInput String
    | TestHypothesisToggleIsLogistic
    | TestHypothesisVectorInput String
    | TestHypothesisExpectedInput String
    | TestHypothesisOutput (Result H.Error (List String))
    | SendLogisticRegressionRequest
    | LGRTSLrInput String
    | LGRTSOInput String
    | LGRTSCInput String
    | LGRTSInput String
    | LGRTSUseAutoLRToggle
    | LogisticRegressionOutput (Result H.Error (List String))
    | FilterInput String
    | SendFilterRequest
    | FilterOutput (Result H.Error String)
    | StartTraining


view : Model -> Html Msg
view model =
    div [ class "container" ]
        [ div [ class "menu" ]
            [ div [ class "menuItem" ] [ text "Preprocessing" ]
            , div [ onClick (ChangeRoute <| PreProcessing ExtractOutput), class "submenuItem" ] [ text "ExtractOutput" ]
            , div [ onClick (ChangeRoute <| PreProcessing MeanNormalize), class "submenuItem" ] [ text "Feature scale and MeanNormalize" ]
            , div [ onClick (ChangeRoute <| PreProcessing AddFeature), class "submenuItem" ] [ text "Add Feature" ]
            , div [ onClick (ChangeRoute <| PreProcessing Filter), class "submenuItem" ] [ text "Filter Training Set" ]

            -- , div [ onClick (ChangeRoute <| PreProcessing InsertX0), class "submenuItem" ] [ text "InsertX0" ]
            , div [ class "menuItem" ] [ text "Learning" ]
            , div [ onClick (ChangeRoute <| Learning LinearRegression), class "submenuItem" ] [ text "Linear Regression" ]
            , div [ onClick (ChangeRoute <| Learning LogisticRegression), class "submenuItem" ] [ text "Logistic Regression" ]
            , div [ class "menuItem" ] [ text "Testing" ]
            , div [ onClick (ChangeRoute <| Test Hypothesis), class "submenuItem" ] [ text "Test Hypothesis" ]
            , div [ class "menuItem" ] [ text "Plotting" ]
            , div [ onClick (ChangeRoute <| Plot), class "submenuItem" ] [ text "Plot" ]
            ]
        , div [ class "route" ] [ renderRoute model ]
        ]


renderRoute : Model -> Html Msg
renderRoute model =
    case model.route of
        Index ->
            div [] []

        PreProcessing r ->
            renderPreprocessingRoute model r

        Learning r ->
            renderLearningRoute model r

        Test r ->
            renderTestRoute model r

        Plot ->
            renderPlotRoute model


renderPlotRoute : Model -> Html Msg
renderPlotRoute model =
    div []
        [ button [ onClick StartTraining ] [ text "Plot" ]
        , div [ id "graph", class "graphContainer" ] []
        ]


renderTestRoute : Model -> TestRoute -> Html Msg
renderTestRoute model r =
    case r of
        Hypothesis ->
            div []
                [ div []
                    [ textarea [ placeholder "Input", value model.testHypoInputM, onInput TestHypothesisInput ] []
                    , textarea [ onInput TestHypothesisVectorInput, value model.testHypoInputTheta, placeholder "Hypothesis Vector" ] []
                    , textarea [ onInput TestHypothesisExpectedInput, value model.testHypoInputExpected, placeholder "Expected output vector" ] []
                    , br [] []
                    , input [ type_ "checkbox", onClick TestHypothesisToggleIsLogistic, checked model.testHypoIsLogistic ] []
                    , label [] [ text "Logistic Regression" ]
                    ]
                , button [ onClick SendTestHypothesisRequest ] [ text "Test" ]
                , if String.length model.testHypoOutputActual > 0 then
                    div []
                        [ span [] [ text ("Error: " ++ model.testHypoOutputError) ]
                        , div [] [ textarea [ value model.testHypoOutputActual ] [] ]
                        ]
                  else
                    span [] []
                ]


renderPreprocessingRoute : Model -> PreProcessingRoute -> Html Msg
renderPreprocessingRoute model r =
    case r of
        Filter ->
            div [ class "vertical-flex" ]
                [ div [ class "text-areas" ]
                    [ textarea [ onInput FilterInput, value model.filterInput ] []
                    , textarea [ value model.filterOutput ] []
                    ]
                , button [ onClick SendFilterRequest ] [ text "Filter" ]
                ]

        MeanNormalize ->
            div [ class "vertical-flex" ]
                [ div [ class "text-areas" ]
                    [ textarea [ placeholder "Source", class "textarea", onInput MeanNormalizeInput, value model.meanNormalizeInput ] []
                    , button [ onClick SendMeanNormalizeRequest ] [ text "Scale and Normalize" ]
                    , if String.length model.meanNormalizeOutput > 0 then
                        textarea [ class "textarea", value model.meanNormalizeOutput ] []
                      else
                        span [] []
                    ]
                ]

        InsertX0 ->
            div [ class "vertical-flex" ]
                [ div [ class "text-areas" ]
                    [ textarea [ onInput InserX0Input, value model.insertX0Input ] []
                    , textarea [ value model.insertX0Output ] []
                    ]
                , button [ onClick SendInsertX0Request ] [ text "Insert" ]
                ]

        ExtractOutput ->
            div [ class "vertical-flex" ]
                [ div []
                    [ textarea [ class "textarea", placeholder "Raw data", onInput ExtractOutputInput, value model.extractOutputInput ] []
                    , input [ onInput ExtractOutputColumn, placeholder "Output column(0 based)", value model.extractOutputColumn ] []
                    , button [ onClick SendExtractOutput ] [ text "Extract" ]
                    , if String.length model.extractOutputTrainingSet > 0 then
                        table [ style [ ( "width", "100%" ) ] ]
                            [ tr []
                                [ td [] [ textarea [ class "small-textarea", placeholder "Training Set", value model.extractOutputTrainingSet ] [] ]
                                , td [] [ textarea [ class "small-textarea", placeholder "Output", value model.extractOutputOutputVector ] [] ]
                                ]
                            ]
                      else
                        span [] []
                    ]
                ]

        AddFeature ->
            div [ class "vertical-flex" ]
                [ div []
                    [ textarea [ placeholder "Source training set", class "small-textarea", onInput AddFeatureTSInput, value model.addFeatureTSInput ] []
                    , input [ placeholder "Source column(0 based)", onInput AddFeatureColumnInput, value model.addFeatureColumnInput ] []
                    , input [ placeholder "Exponent", onInput AddFeatureExpInput, value model.addFeatureExpInput ] []
                    , button [ onClick SendAddFeatureRequest ] [ text "Add Feature" ]
                    , if String.length model.addFeatureOutput > 0 then
                        textarea [ class "small-textarea", placeholder "Training Set", value model.addFeatureOutput ] []
                      else
                        span [] []
                    ]
                ]


renderLearningRoute : Model -> LearningRoute -> Html Msg
renderLearningRoute model r =
    case r of
        LinearRegression ->
            div [ class "vertical-flex" ]
                [ table [ style [ ( "width", "100%" ) ] ]
                    [ tr []
                        [ td []
                            [ textarea [ class "textarea", onInput LRTSInput, placeholder "Training set", value model.lrtsInput ] []
                            , textarea [ class "textarea", onInput LRTSOInput, placeholder "Output", value model.lrtsOInput ] []
                            , div []
                                [ input [ type_ "checkbox", onClick LRTSUseNormalEqToggle, checked model.lrtsUseNormalEq ] []
                                , span [] [ text "Use Normal Equation" ]
                                ]
                            , if model.lrtsUseNormalEq then
                                span [] []
                              else
                                div []
                                    [ textarea
                                        [ class "textarea", onInput LRTSCInput, placeholder "Initial coeff", value model.lrtsCInput ]
                                        []
                                    , div [] [ input [ type_ "checkbox", onClick LRTSUseAutoLRToggle, checked model.lrtsLrAuto ] [], text "Auto detect learning rate" ]
                                    , if model.lrtsLrAuto then
                                        span [] []
                                      else
                                        input
                                            [ placeholder "Learning rate", onInput LRTSLrInput, value model.lrtsLr ]
                                            []
                                    ]
                            , button [ onClick SendLinearRegressionRequest ] [ text "Run Linear Regression" ]
                            ]
                        ]
                    , tr []
                        [ if String.length model.lrCoeff > 0 then
                            td []
                                [ span [] [ text "Error : " ]
                                , span [] [ text model.lrError ]
                                , table []
                                    [ tr []
                                        [ td [] [ textarea [ placeholder "Result Hypothesis", class "small-textarea", value model.lrCoeff ] [] ]
                                        , if String.length model.lrLog > 0 then
                                            td [] [ textarea [ style [ ( "height", "500px" ) ], placeholder "Gradient descent log", class "small-textarea", value model.lrLog ] [] ]
                                          else
                                            span [] []
                                        ]
                                    ]
                                ]
                          else
                            span [] []
                        ]
                    ]
                ]

        LogisticRegression ->
            div [ class "vertical-flex" ]
                [ table [ style [ ( "width", "100%" ) ] ]
                    [ tr []
                        [ td []
                            [ textarea [ class "textarea", onInput LGRTSInput, placeholder "Training set", value model.lgrtsInput ] []
                            , textarea [ class "textarea", onInput LGRTSOInput, placeholder "Output", value model.lgrtsOInput ] []
                            , div
                                []
                                [ textarea [ class "textarea", onInput LGRTSCInput, placeholder "Initial coeff", value model.lgrtsCInput ] []
                                , div [] [ input [ type_ "checkbox", onClick LGRTSUseAutoLRToggle, checked model.lgrtsLrAuto ] [], text "Auto detect learning rate" ]
                                , if model.lgrtsLrAuto then
                                    span [] []
                                  else
                                    input
                                        [ placeholder "Learning rate", onInput LGRTSLrInput, value model.lgrtsLr ]
                                        []
                                ]
                            , button [ onClick SendLogisticRegressionRequest ] [ text "Run Logistic Regression" ]
                            ]
                        ]
                    , tr []
                        [ if String.length model.lgrCoeff > 0 then
                            td []
                                [ table []
                                    [ tr []
                                        [ td [] [ textarea [ placeholder "Result Hypothesis", class "small-textarea", value model.lgrCoeff ] [] ]
                                        , if String.length model.lgrLog > 0 then
                                            td [] [ textarea [ style [ ( "height", "500px" ) ], placeholder "Gradient descent log", class "small-textarea", value model.lgrLog ] [] ]
                                          else
                                            span [] []
                                        ]
                                    ]
                                , span [] [ text "Error : " ]
                                , span [] [ text model.lgrError ]
                                ]
                          else
                            span [] []
                        ]
                    ]
                ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangeRoute r ->
            ( { model | route = r }, Cmd.none )

        MeanNormalizeInput s ->
            ( { model | meanNormalizeInput = s }, Cmd.none )

        MeanNormalizeOutput (Err e) ->
            ( model, Cmd.none )

        MeanNormalizeOutput (Ok o) ->
            ( { model | meanNormalizeOutput = o }, Cmd.none )

        SendMeanNormalizeRequest ->
            ( model, H.send MeanNormalizeOutput <| H.post "/meanNormalize" (H.jsonBody <| Encode.string model.meanNormalizeInput) Decode.string )

        ExtractOutputInput s ->
            ( { model | extractOutputInput = s }, Cmd.none )

        ExtractOutputColumn s ->
            ( { model | extractOutputColumn = s }, Cmd.none )

        SendExtractOutput ->
            ( model, H.send ExtractOutputOutput <| H.post "/extractOutput" (H.jsonBody <| Encode.list [ Encode.string model.extractOutputInput, Encode.string model.extractOutputColumn ]) (Decode.list Decode.string) )

        ExtractOutputOutput (Err _) ->
            ( model, Cmd.none )

        ExtractOutputOutput (Ok [ ts, o ]) ->
            ( { model | extractOutputTrainingSet = ts, extractOutputOutputVector = o }, Cmd.none )

        ExtractOutputOutput (Ok _) ->
            ( model, Cmd.none )

        SendInsertX0Request ->
            ( model, H.send InsertX0Output <| H.post "/insertX0" (H.jsonBody <| Encode.string model.insertX0Input) Decode.string )

        InserX0Input x ->
            ( { model | insertX0Input = x }, Cmd.none )

        InsertX0Output (Ok ts) ->
            ( { model | insertX0Output = ts }, Cmd.none )

        InsertX0Output (Err _) ->
            ( model, Cmd.none )

        LRTSInput s ->
            ( { model | lrtsInput = s }, Cmd.none )

        LRTSOInput s ->
            ( { model | lrtsOInput = s }, Cmd.none )

        LRTSCInput s ->
            ( { model | lrtsCInput = s }, Cmd.none )

        LRTSLrInput s ->
            ( { model | lrtsLr = s }, Cmd.none )

        LRTSUseNormalEqToggle ->
            ( { model | lrtsUseNormalEq = not model.lrtsUseNormalEq }, Cmd.none )

        LRTSUseAutoLRToggle ->
            ( { model | lrtsLrAuto = not model.lrtsLrAuto }, Cmd.none )

        SendLinearRegressionRequest ->
            ( model, H.send LinearRegressionOutput <| H.post "/linearRegression" (H.jsonBody <| Encode.list [ Encode.string model.lrtsInput, Encode.string model.lrtsOInput, Encode.string model.lrtsCInput, Encode.string model.lrtsLr, Encode.string <| toString model.lrtsUseNormalEq, Encode.string <| toString model.lrtsLrAuto ]) (Decode.list Decode.string) )

        LinearRegressionOutput (Ok [ err, coeff ]) ->
            ( { model | lrError = err, lrCoeff = coeff }, Cmd.none )

        LinearRegressionOutput (Ok [ err, coeff, log ]) ->
            ( { model | lrError = err, lrCoeff = coeff, lrLog = log }, Cmd.none )

        LinearRegressionOutput (Ok _) ->
            log "asdasd" <| ( model, Cmd.none )

        LinearRegressionOutput (Err _) ->
            ( model, Cmd.none )

        AddFeatureTSInput s ->
            ( { model | addFeatureTSInput = s }, Cmd.none )

        AddFeatureColumnInput s ->
            ( { model | addFeatureColumnInput = s }, Cmd.none )

        AddFeatureExpInput s ->
            ( { model | addFeatureExpInput = s }, Cmd.none )

        SendAddFeatureRequest ->
            ( model, H.send AddFeatureOutput <| H.post "/addFeature" (H.jsonBody <| Encode.list [ Encode.string model.addFeatureTSInput, Encode.string model.addFeatureColumnInput, Encode.string model.addFeatureExpInput ]) Decode.string )

        AddFeatureOutput (Ok s) ->
            ( { model | addFeatureOutput = s }, Cmd.none )

        AddFeatureOutput (Err _) ->
            ( model, Cmd.none )

        SendTestHypothesisRequest ->
            ( model, H.send TestHypothesisOutput <| H.post "/testHypothesis" (H.jsonBody <| Encode.list [ Encode.string model.testHypoInputM, Encode.string model.testHypoInputTheta, Encode.string model.testHypoInputExpected, Encode.string <| toString model.testHypoIsLogistic ]) (Decode.list Decode.string) )

        TestHypothesisOutput (Ok [ output, error ]) ->
            ( { model | testHypoOutputActual = output, testHypoOutputError = error }, Cmd.none )

        TestHypothesisOutput (Ok _) ->
            ( model, Cmd.none )

        TestHypothesisOutput (Err _) ->
            ( model, Cmd.none )

        TestHypothesisInput s ->
            ( { model | testHypoInputM = s }, Cmd.none )

        TestHypothesisVectorInput s ->
            ( { model | testHypoInputTheta = s }, Cmd.none )

        TestHypothesisExpectedInput s ->
            ( { model | testHypoInputExpected = s }, Cmd.none )

        TestHypothesisToggleIsLogistic ->
            ( { model | testHypoIsLogistic = not model.testHypoIsLogistic }, Cmd.none )

        LGRTSLrInput s ->
            ( { model | lgrtsLr = s }, Cmd.none )

        LGRTSOInput s ->
            ( { model | lgrtsOInput = s }, Cmd.none )

        LGRTSCInput s ->
            ( { model | lgrtsCInput = s }, Cmd.none )

        LGRTSInput s ->
            ( { model | lgrtsInput = s }, Cmd.none )

        SendLogisticRegressionRequest ->
            ( model, H.send LogisticRegressionOutput <| H.post "/logisticRegression" (H.jsonBody <| Encode.list [ Encode.string model.lgrtsInput, Encode.string model.lgrtsOInput, Encode.string model.lgrtsCInput, Encode.string model.lgrtsLr, Encode.string <| toString model.lgrtsLrAuto ]) (Decode.list Decode.string) )

        LGRTSUseAutoLRToggle ->
            ( { model | lgrtsLrAuto = not model.lgrtsLrAuto }, Cmd.none )

        LogisticRegressionOutput (Ok [ error, hypo, log ]) ->
            ( { model | lgrCoeff = hypo, lgrLog = log, lgrError = error }, Cmd.none )

        LogisticRegressionOutput _ ->
            ( model, Cmd.none )

        FilterInput s ->
            ( { model | filterInput = s }, Cmd.none )

        SendFilterRequest ->
            ( model, H.send FilterOutput <| H.post "/filter" (H.jsonBody <| Encode.string model.filterInput) Decode.string )

        FilterOutput (Ok s) ->
            ( { model | filterOutput = s }, Cmd.none )

        FilterOutput _ ->
            ( model, Cmd.none )

        StartTraining ->
            ( model, plot "" )


convertData : List (List Float) -> List ( Float, Maybe Float, Maybe Float )
convertData d =
    let
        fn : List Float -> ( Float, Maybe Float, Maybe Float )
        fn d =
            case d of
                [ x, y, f ] ->
                    if f == 1 then
                        ( x, Just y, Nothing )
                    else
                        ( x, Nothing, Just y )

                _ ->
                    crash "Unexpected list"
    in
        List.map fn d


generateData : List ( Float, Float, Float )
generateData =
    let
        generateColumn : Float -> List ( Float, Float, Float )
        generateColumn x =
            List.map (\y -> ( x, (toFloat y) * 100, (toFloat y) * 50 )) <| List.range 0 10
    in
        List.foldl (\x a -> (generateColumn <| toFloat (x * 100)) ++ a) [] (List.range 0 10)


init : ( Model, Cmd Msg )
init =
    ( { route = Index
      , meanNormalizeInput = ""
      , meanNormalizeOutput = ""
      , extractOutputInput = ""
      , extractOutputColumn = ""
      , extractOutputTrainingSet = ""
      , extractOutputOutputVector = ""
      , insertX0Output = ""
      , insertX0Input = ""
      , lrtsInput = ""
      , lrtsOInput = ""
      , lrtsCInput = ""
      , lrtsUseNormalEq = False
      , lrtsLrAuto = False
      , lrCoeff = ""
      , lrtsLr = ""
      , lrError = ""
      , lrLog = ""
      , addFeatureTSInput = ""
      , addFeatureColumnInput = ""
      , addFeatureExpInput = ""
      , addFeatureOutput = ""
      , testHypoInputM = ""
      , testHypoInputTheta = ""
      , testHypoInputExpected = ""
      , testHypoOutputActual = ""
      , testHypoOutputError = ""
      , testHypoIsLogistic = False
      , lgrtsInput = ""
      , lgrtsOInput = ""
      , lgrtsCInput = ""
      , lgrtsLrAuto = False
      , lgrtsLr = ""
      , lgrCoeff = ""
      , lgrError = ""
      , lgrLog = ""
      , filterInput = ""
      , filterOutput = ""
      }
    , Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch []


main : Program Never Model Msg
main =
    program
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }
