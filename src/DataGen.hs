module DataGen where

import Lib
import qualified Data.Vector as V
import System.Random
import Codec.Picture
import Codec.Picture.Types
import NeuralNet
import Data.HashMap.Strict as HM
import HMatrixStatic
import Debug.Trace
import GHC.TypeNats
import GHC.TypeLits
import Data.Vector hiding (zipWith, mapM_)
import LogisticRegression

data DataGenerator a b = DataGenerator
  { grid :: TMatrix a 2
  , inputs :: TMatrix b 2
  , scale :: Double -> Double -> (Int, Int)
  }

getDataGenerator :: Double -> IO (DataGenerator a b)
getDataGenerator scale = do
  let g = tmatrix $ V.fromList $ do
      let
        start = (-200 :: Double) * scale;
        offset = 5 * scale;
      x <- Prelude.take 80 [start, start + offset ..]
      y <- Prelude.take 80 [start, start + offset ..]
      return $ V.fromList [x, y]
  is <- generateInputs 1000 2 200
  return $ DataGenerator 
    { grid = g
    , inputs = is
    , scale = (\x y -> (round $ x/scale, round $ y/scale))
    }

data PointType = GradientOn Double | GradientOff Double | DataPointOn | DataPointOff deriving (Eq)

generateInputs :: Int -> Int -> Float -> IO (TMatrix r c)
generateInputs rows cols range = fmap tmatrix $ V.replicateM rows $ V.replicateM cols $ (realToFrac <$> randomRIO (-range,range::Float) :: IO Double)

generateGrid :: TMatrix 6400 2
generateGrid = tmatrix $ V.fromList $ do
      x <- Prelude.take 80 [(-200 :: Double), -195..]
      y <- Prelude.take 80 [(-200 :: Double), -195..]
      return $ V.fromList [x, y]

divider :: Double -> Double -> Bool
divider x y = (x - 50) ^2+ (y-50)^2 < (100 * 100)

applyDivider :: (Double -> Double -> Bool) -> TMatrix m 2 -> TMatrix m 1
applyDivider divider ts =  fn $ unr (toTaggedRows ts)
  where
    fn :: [TVector c] -> TMatrix m 1
    fn x = fromVector $ tvector $ V.fromList $ fmap fn2 x
    fn2 :: TVector c -> Double
    fn2 i =  let [x, y] = unr $ unr i in if divider x y  then 1 else 0

convertToDataPoints :: TMatrix m 2  -> TMatrix m 1 -> (Double -> PointType) -> (Double -> PointType) -> [(Double, Double, PointType)]
convertToDataPoints i o on off = let
  rows = fmap (unr.unr) $ unr $ toTaggedRows i
  os  = fmap (unr.unr) $ unr $ toTaggedRows o
  fn :: [Double] -> [Double] -> (Double, Double, PointType)
  fn [x, y] [z] = (x, y, if z > 0.5 then on z else off z)
  in zipWith fn rows os

makeDivider :: NeuralNet i o h p -> (Double -> Double -> Bool)
makeDivider nn = fn
  where
    fn :: Double -> Double -> Bool
    fn x y = let
      [o] = unr $ unr $ passThrough nn $ tvector $ V.fromList [x, y]  in o > 0.5

getPlot2 :: NeuralNet 2 1 h p -> TMatrix m 2 -> TMatrix m 1 -> TMatrix m1 2 -> TMatrix m1 1 -> (Double -> Double -> (Int, Int)) -> IO DynamicImage
getPlot2 nn gin gout ain aout scale = let
  in do
    mi <- createMutableImage 450 450 (PixelRGB8 0 0 0)
    mapM_ (addDataPoint mi) $ convertToDataPoints gin gout GradientOn GradientOff
    mapM_ (addDataPoint mi) $ convertToDataPoints ain aout (\_ -> DataPointOn) (\_ -> DataPointOff)
    ImageRGB8 <$> freezeImage mi
  where
  addDataPoint  mi (x, y, z) = do
    let (x1, y1) = scale x y
    writePixel mi (x1 + 225) (y1 + 225) color
    where
      color = case z of
        GradientOff z1 -> PixelRGB8 0 255 0
        GradientOn z1 -> PixelRGB8 255 0 0
        -- GradientOn z1 -> PixelRGB8 (100 + (round $ 255 * z1)) 0 0
        DataPointOn -> PixelRGB8 255 255 0
        DataPointOff -> PixelRGB8 0 0 0

getNetwork :: NeuralNet 2 1 1 3
getNetwork = let
  iLayer :: Layer 3 2
  iLayer = Layer { weights = (tmatrix $ V.fromList 
                              [ V.fromList [0.0044, 0.0011, 0.0021]
                              , V.fromList [0.0023, 0.0065, 0.0034]
                              , V.fromList [0.0025, 0.0054, 0.0012]
                              ] :: TMatrix 3 3), zs = Nothing, activations = Nothing }
  lhLayer :: Layer 1 3
  lhLayer = Layer { weights = ( tmatrix $ V.fromList 
                            [ V.fromList [0.0035, 0.0012, 0.0066, 0.0021]
                            ] :: TMatrix 1 4), zs = Nothing, activations = Nothing }
  in NeuralNet { inputLayer = iLayer, hiddenLayers = TaggedList [], lastHiddenLayer = lhLayer, outputLayer = Nothing, activation = sigmoid, pActivation = pSigmoid }

getNetwork2 :: NeuralNet 2 1 1 3
getNetwork2 = let
  iLayer :: Layer 3 2
  iLayer = Layer { weights = (tmatrix $ V.fromList 
                              [ V.fromList [0.0044, 0.0011, 0.0021]
                              , V.fromList [0.0023, 0.0065  + 0.000001, 0.0034]
                              , V.fromList [0.0025, 0.0054, 0.0012]
                              ] :: TMatrix 3 3), zs = Nothing, activations = Nothing }
  lhLayer :: Layer 1 3
  lhLayer = Layer { weights = ( tmatrix $ V.fromList 
                            [ V.fromList [0.0035, 0.0012, 0.0066, 0.0021]
                            ] :: TMatrix 1 4), zs = Nothing, activations = Nothing }
  in NeuralNet { inputLayer = iLayer, hiddenLayers = TaggedList [], lastHiddenLayer = lhLayer, outputLayer = Nothing, activation = sigmoid, pActivation = pSigmoid }

getNetwork3 :: NeuralNet 2 1 1 3
getNetwork3 = let
  iLayer :: Layer 3 2
  iLayer = Layer { weights = (tmatrix $ V.fromList 
                              [ V.fromList [0.0044, 0.0011, 0.0021]
                              , V.fromList [0.0023, 0.0065  - 0.000001, 0.0034]
                              , V.fromList [0.0025, 0.0054, 0.0012]
                              ] :: TMatrix 3 3), zs = Nothing, activations = Nothing }
  lhLayer :: Layer 1 3
  lhLayer = Layer { weights = ( tmatrix $ V.fromList 
                            [ V.fromList [0.0035, 0.0012, 0.0066, 0.0021]
                            ] :: TMatrix 1 4), zs = Nothing, activations = Nothing }
  in NeuralNet { inputLayer = iLayer, hiddenLayers = TaggedList [], lastHiddenLayer = lhLayer, outputLayer = Nothing, activation = sigmoid, pActivation = pSigmoid }


randomNetwork
  :: forall i o p h. (KnownNat i, KnownNat o, KnownNat p, KnownNat h)
  => (Double -> Double)
  -> (Double -> Double) 
  -> Int
  -> Int
  -> Int
  -> Int
  -> IO (NeuralNet i o h p)
randomNetwork act pact inputUnits outputUnits hc unitsPerHidden  = do
  let
  inputLayer <- getRandomLayer inputUnits unitsPerHidden :: IO (Layer p i)
  hiddenLayers <- V.replicateM (hc - 1) $ getRandomLayer unitsPerHidden unitsPerHidden :: IO (V.Vector (Layer p p))
  lastHiddenLayer <- getRandomLayer unitsPerHidden outputUnits :: IO (Layer o p)
  return $ NeuralNet
    { inputLayer = inputLayer
    , hiddenLayers = TaggedList (V.toList hiddenLayers)
    , lastHiddenLayer = lastHiddenLayer
    , activation = act
    , pActivation = pact
    , outputLayer = Nothing
    }
  where
    getRandomLayer :: Int -> Int -> IO (Layer a b)
    getRandomLayer n o =  (\x -> Layer (tmatrix x)  Nothing Nothing) <$> (V.replicateM o $ getRandomWeights (n+1))
    getRandomWeights :: Int -> IO (Vector Double)
    getRandomWeights c = V.replicateM c $ (realToFrac <$> randomRIO (-1,1::Float) :: IO Double)

