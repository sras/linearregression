{-# OPTIONS_GHC -fplugin GHC.TypeLits.Normalise #-}

module NeuralNet where

import HMatrixStatic
import GHC.TypeNats
import LogisticRegression
import Data.Proxy
import Data.Traversable
import System.Random
import qualified Numeric.LinearAlgebra as LA
import Control.Monad
import Data.Vector as V hiding (foldl) 
import Text.InterpolatedString.Perl6 (qc)
import Debug.Trace
import Lib (looper, looperObserver)
import Data.Maybe
import Data.Aeson as Aeson
import Data.ByteString.Lazy (ByteString, toStrict)
import Data.Text.Encoding as TE
import qualified Data.Text as T
import Control.Concurrent.Chan
import Control.DeepSeq
import GHC.Generics

data OutputLayer o = OutputLayer { outputZs :: TVector o, output:: TVector o } deriving (Show, Generic, Read, NFData)

data Layer (o :: Nat)  (n :: Nat) =
  Layer { weights :: (TMatrix o (1+n)), zs :: Maybe (TVector n), activations :: Maybe (TVector (n+1)) } deriving (Show, Read, Generic, NFData)
  -- zs is for remembering weighted sum of inputs, ie the 'z' in g(z).
  -- While doing forword prop, we carry the 'z' from each step forward
  -- so that the zs field of the next layer is updated with the proper value
data NeuralNet (i :: Nat) (o :: Nat) (h :: Nat) (p :: Nat) =
  NeuralNet { inputLayer :: Layer p i
            , hiddenLayers :: TaggedList (h-1) (Layer p p)
            , lastHiddenLayer :: (Layer o p)
            , outputLayer :: Maybe (OutputLayer o)
            , activation :: Double -> Double
            , pActivation :: Double -> Double
            } deriving (Generic, NFData)

instance Show (NeuralNet i o p h) where
  show nn = T.unpack $ TE.decodeUtf8 $ toStrict $ Aeson.encode  nn

instance Aeson.ToJSON (NeuralNet i o p h) where
  toJSON nn = Aeson.object 
    [ "inputLayer" .= (show $ inputLayer nn)
    , "hiddenLayers" .= (show $ hiddenLayers nn)
    , "lastHiddenLayer" .= (show $ lastHiddenLayer nn)
    , "outputLayer" .= (show $ outputLayer nn)
    ]

instance Aeson.FromJSON (NeuralNet i o p h) where
  parseJSON (Object o) = do
    inputLayer <- o .: "inputLayer"
    hiddenLayers <- o .: "hiddenLayers"
    lastHiddenLayer <- o .: "lastHiddenLayer"
    return $ NeuralNet (read inputLayer) (read hiddenLayers) (read lastHiddenLayer) Nothing sigmoid pSigmoid

computeNextZ :: Layer o n -> TVector o
computeNextZ l = case activations l of
  Just a -> (weights l) #> a
  Nothing -> error "No activation found"

computeLayer :: NeuralNet a b c d -> Layer n e -> Layer o n -> Layer o n
computeLayer nn player layer = let
  (z, a) = propogateSingle nn player in layer {zs = Just z, activations = Just $ vcons 1 a}

propogateSingle :: NeuralNet a b c d -> Layer n e -> (TVector n, TVector n)
propogateSingle nn player = let
    z = computeNextZ player
    a = applyActivation nn z
  in (z, a)

applyActivation :: NeuralNet i o h p -> TVector a -> TVector a
applyActivation nn i = cmap (activation nn) i

doForwardPropogation :: forall i o h p. NeuralNet i o h p -> TVector i -> NeuralNet i o h p
doForwardPropogation nn i = let
    activationSet = nn { inputLayer = (inputLayer nn) { activations = Just $ vcons 1 $ i } }
  in propogate activationSet
  where
  compute :: Layer n1 a1 -> Layer o1 n1 -> Layer o1 n1
  compute = computeLayer nn
  propogate nn =
    case unr $ hiddenLayers nn of
      [] ->  let
          lHLayer :: Layer o p = compute (inputLayer nn) (lastHiddenLayer nn)
          (outputz, output) = propogateSingle nn lHLayer
          outputL = OutputLayer { outputZs = outputz, output = output }
        in nn { lastHiddenLayer = lHLayer, outputLayer = Just outputL }
      (h:xh) -> let
          firstHiddenLayer = compute (inputLayer nn) h
          newHLayers = Prelude.reverse $ Prelude.foldl fn [firstHiddenLayer] xh
            where
              fn :: [Layer p p] -> Layer p p -> [Layer p p]
              fn r@(l1:_) l = (compute l1 l):r
          lHLayer :: Layer o p = compute (Prelude.last newHLayers) (lastHiddenLayer nn)
          (outputz, output) = propogateSingle nn lHLayer
          outputL = OutputLayer { outputZs = outputz, output = output }
        in nn { hiddenLayers = TaggedList newHLayers, lastHiddenLayer = lHLayer, outputLayer = Just outputL }

countInput :: Layer o n -> Int
countInput l = cols $ weights l

countOutput :: Layer o n -> Int
countOutput l = rows $ weights l

passThrough :: NeuralNet i o h p -> TVector i -> TVector o
passThrough nn i = let nn1 = doForwardPropogation nn i in output $ fromJust $ outputLayer nn1

type CDeltaPair i o h p = (TMatrix p (i+1), TaggedList (h-1) (TMatrix p (p+1)), TMatrix o (p + 1))

partialDerivatives
  :: forall i o p h.NeuralNet i o h p
  -> (TaggedList h (TVector p), TVector o)
  -> CDeltaPair i o h p
partialDerivatives nn (hiddenDeltas, outputDelta) = let
  firstHiddenLayerDelta :: TVector p = Prelude.head $ unr hiddenDeltas
  iLayer :: Layer p i = inputLayer nn
  inputLayerCDelta :: TMatrix p (i+1) = partialDerivativesForLayer iLayer firstHiddenLayerDelta
  hiddenCDeltas = taggedZipWith partialDerivativesForLayer (hiddenLayers nn) (taggedTail hiddenDeltas)
  lastHiddenCDelta = partialDerivativesForLayer (lastHiddenLayer nn) outputDelta
  in (inputLayerCDelta, hiddenCDeltas, lastHiddenCDelta)
  where
  partialDerivativesForLayer
    :: forall i o. Layer o i
    -> TVector o
    -> TMatrix o (i+1)
  partialDerivativesForLayer thisLayer nextDelta  = let
    inputActivationTranspose :: TMatrix 1 (i+1) = tr (fromVector $ fromJust $ activations thisLayer)
    thisLayerCDelta :: TMatrix o (i+1) = (fromVector nextDelta) <> (inputActivationTranspose)
    in thisLayerCDelta

computeLayerErrors
  :: forall i o p h.NeuralNet i o h p
  -> TVector o
  -> (TaggedList h (TVector p), TVector o)
computeLayerErrors nn e = (TaggedList hlErrors, outputError)
  where
    outputError = case (outputLayer nn) of
      Just o -> (output o) - e
      Nothing -> error "Network does not have result"
    lastHLError :: TVector p  = getLayerError outputError $ lastHiddenLayer nn
    hiddenLayersReversed :: [Layer p p] = let (TaggedList x) = hiddenLayers nn in Prelude.reverse x
    hlErrors :: [TVector p] = let
      fn :: [TVector p] -> Layer p p -> [TVector p]
      fn r@(le:_) l = (getLayerError le l):r
      in foldl fn [lastHLError] hiddenLayersReversed
    getLayerError :: forall o1 p1. TVector o1 -> Layer o1 p1 -> TVector p1
    getLayerError lastError l = let
      wt :: TMatrix (p1+1) o1 = tr $ weights l
      in case activations l of
        Just acts -> dropZero $ (wt #> lastError) * (acts * (1 - acts)) -- dropZero discards error associated with bias nodes.
        Nothing -> error "No zs"

computeCost
  :: forall i o p h m.NeuralNet i o h p
  -> TaggedList m (TVector i, TVector o)
  -> Double
  -> Double
computeCost nn taggedPairs rp = let
  pairs = unr $ taggedPairs
  m =  fromIntegral $ Prelude.length $ pairs
  doubleSum = foldl fn1 0 pairs
  squaredSumOfWeights =
    (squareSumLayerWeights $ inputLayer nn) +
    (foldl (\x l -> x + (squareSumLayerWeights l)) 0 $ (unr $ hiddenLayers nn)) +
    (squareSumLayerWeights $ lastHiddenLayer nn)
  in (-doubleSum/m) + ((rp/(2*m))*squaredSumOfWeights)
  where
    squareSumLayerWeights :: Layer o1 n1 -> Double
    squareSumLayerWeights l = LA.sumElements $ ((unr $ dropColumn $ weights l) ^ 2)
    fn1 :: Double -> (TVector i, TVector o) -> Double
    fn1 e i = e + (errorForInput i)
      where
        errorForInput :: (TVector i, TVector o) -> Double
        errorForInput (i, e) = let
          nout :: TVector o = passThrough nn i
          in LA.sumElements $ unr $ (e * (cmap log nout)) + ((1 - e) * (cmap log (1 - nout)))

trainNetwork
  :: forall i o p h m.NeuralNet i o h p
  -> TMatrix m i
  -> TMatrix m o
  -> Double
  -> Double
  -> Int
  -> Int
  -> ((Int, Maybe (NeuralNet i o h p, Double)) -> IO ())
  -> IO (NeuralNet i o h p)
trainNetwork nn i e slr reg bc pb observer = let
  inputs = toTaggedRows $ i
  expected = toTaggedRows e
  pairs = (taggedZip inputs expected)
  in do
    r <- trainBatch bc pb nn pairs slr
    observer (0, Nothing)
    return r
  where
  inputCount = rows i
  trainBatch :: Int -> Int ->  NeuralNet i o h p -> TaggedList m (TVector i, TVector o) -> Double -> IO (NeuralNet i o h p)
  trainBatch 0 _ nn _ _ = return nn
  trainBatch bc pb nn pairs lr = do
    let currentCost = computeCost nn pairs reg
    trainedNN <- looperObserver (trainOnce pairs lr) nn (\_ -> return ()) pb
    let newCost = computeCost trainedNN pairs reg
    observer (bc, Just (trainedNN, newCost))
    if newCost < currentCost
      then do
        trainBatch (bc-1) pb trainedNN pairs (lr*1.5)
      else trainBatch bc pb nn pairs (lr/2)
  computeDerivativeSum :: TaggedList m (TVector i, TVector o) -> Double -> NeuralNet i o h p -> CDeltaPair i o h p
  computeDerivativeSum pairs@(TaggedList ts) lr nn = let
    in foldl (derivativesForInput nn) zeroedDelta ts
  trainOnce :: TaggedList m (TVector i, TVector o) -> Double -> NeuralNet i o h p -> NeuralNet i o h p
  trainOnce pairs lr nn = let
    (ilPd, hlPds, lhPd) = computeDerivativeSum pairs lr nn
    in nn 
      { inputLayer = updateLayer (inputLayer nn) ilPd
      , hiddenLayers = taggedZipWith updateLayer (hiddenLayers nn) hlPds
      , lastHiddenLayer = updateLayer (lastHiddenLayer nn) lhPd
      }
    where
      updateLayer :: forall o1 i1.Layer o1 i1 -> TMatrix o1 (i1+1) -> Layer o1 i1
      updateLayer layer partialDerivativesSum = let
        learningRate :: TMatrix o1 (i1+1) = realToFrac lr
        layerWeights = weights layer
        finalDerivatives = (computeFinalDerivatives partialDerivativesSum)
        in layer { weights = layerWeights - learningRate * finalDerivatives }
        where
          colCount = countInput layer
          rowCount = countOutput layer
          computeFinalDerivatives :: TMatrix o1 (i1+1)  -> TMatrix o1 (i1+1)
          computeFinalDerivatives derivatives = (derivatives/(realToFrac inputCount)) + (mask * (regularizationParam * (weights layer)))
            where
            regularizationParam = realToFrac reg
            mask :: TMatrix o1 (i1+1)
            mask = TMatrix $ LA.repmat (1 LA.>< colCount $ (0:[1,1..])) rowCount 1
  zeroedDelta :: CDeltaPair i o h p
  zeroedDelta = let
    i = countInput $ inputLayer nn
    p = countOutput $ inputLayer nn
    o = countOutput $ lastHiddenLayer nn
    h = (Prelude.length $ unr $ hiddenLayers nn)
    a1 = tmatrixZeros p i
    a3 = tmatrixZeros o (p+1)
    a2 = Prelude.take h $ Prelude.repeat $ tmatrixZeros p (p+1) in (a1, TaggedList $ a2, a3)
  derivativesForInput :: NeuralNet i o h p -> CDeltaPair i o h p -> (TVector i, TVector o) -> CDeltaPair i o h p
  derivativesForInput nn (a1, a2, a3) (i, o) = let
    (d1, d2, d3) =  doBackPropogation (doForwardPropogation nn i) o
    in (a1 + d1, taggedZipWith (+) a2 d2, a3 + d3)
  doBackPropogation :: NeuralNet i o h p -> TVector o -> CDeltaPair i o h p
  doBackPropogation nn o = partialDerivatives nn $ computeLayerErrors nn o

computeGradient
  :: forall i o p h m.NeuralNet i o h p
  -> TaggedList m (TVector i, TVector o)
  -> Double
  -> [LA.Matrix Double]
computeGradient nn pairs rp = let
  eps = 0.000001
  computeCost' nn = computeCost nn pairs rp
  layerGradient :: Int -> LA.Matrix Double
  layerGradient lc = let
    layer = getLayerByIndex lc
    lCols = LA.cols $ layer
    peps = LA.matrix lCols $ computeCost' <$> ((replaceLayerByIndex lc) <$> (getLayerStream eps $ layer))
    meps = LA.matrix lCols $ computeCost' <$> ((replaceLayerByIndex lc) <$> (getLayerStream (-eps) $ layer))
    in let x = (realToFrac $ 2 * eps) in (peps - meps)/x
  getLayerByIndex :: Int -> LA.Matrix Double
  getLayerByIndex idx =
    if idx == 0 then unr $ weights $ inputLayer nn else
      let hlIndex = idx - 1
        in if hlIndex < (Prelude.length hls) then (unr $ weights $ hls !! hlIndex) else (unr $ weights $ lastHiddenLayer nn)
  hls = unr $ hiddenLayers nn
  replaceLayerByIndex :: Int -> LA.Matrix Double -> NeuralNet i o h p
  replaceLayerByIndex idx wts  = 
    if idx == 0 then nn { inputLayer = let l = inputLayer nn in l { weights = TMatrix wts } } else
      let hlIndex = idx - 1
        in if hlIndex < (Prelude.length hls) then nn { hiddenLayers = TaggedList (replaceIthLayerWeight hls hlIndex wts) } 
        else nn { lastHiddenLayer = (lastHiddenLayer nn) { weights = TMatrix wts} }
  replaceIthLayerWeight :: [Layer a b] -> Int -> LA.Matrix Double -> [Layer a b]
  replaceIthLayerWeight [] _ _ = []
  replaceIthLayerWeight (x:xs) 0 wts = x { weights = TMatrix wts } : xs
  replaceIthLayerWeight a n wts = replaceIthLayerWeight a (n-1) wts
  getLayerStream :: Double -> LA.Matrix Double -> [LA.Matrix Double]
  getLayerStream eps weights = let
    r = LA.rows $ weights
    c = LA.cols $ weights
    updateElement :: (Int, Int) -> LA.Matrix Double
    updateElement (x, y) = (LA.accum weights (+) [((x, y), eps)] )
    r1 = updateElement <$> (getPairs r c)
    in r1
  getPairs :: Int -> Int -> [(Int, Int)]
  getPairs r c = do
    x <- [0..r-1]
    y <- [0.. c-1]
    return (x, y)
  in layerGradient <$> [0,1]
