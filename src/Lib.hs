module Lib where

import qualified Numeric.LinearAlgebra as LA
import Data.Decimal
import qualified Data.Text as T
import qualified System.IO.Strict as S
import Debug.Trace
import Data.Monoid
import Data.List (unfoldr)
import qualified Data.Attoparsec.Text as PR
import Data.Vector as V
import Prelude  (show, (==), pure, undefined, read, reads, fromIntegral, error, (/=), (-), (/), (*), (+), writeFile, fmap, (.), ($), (<$>), IO, FilePath, Int, String, Either(..), Float)
import qualified Prelude as P
import Data.Proxy
import GHC.TypeNats
import qualified Data.Text.IO as DTIO
import System.Random
import Prelude
import Control.Concurrent.Chan
import Control.DeepSeq

data Term = Term { coeff :: P.Double, exp :: Int } deriving (P.Show)

data Hypothesis = Hypothesis { terms :: [Term] } deriving (P.Show)

type RawTrainingItem = [T.Text]

type TrainingItem = Vector P.Double

type TrainingSet = Vector TrainingItem

readDouble :: T.Text -> P.Double
readDouble t = let
  trimmed = T.strip t
  tstr = T.unpack trimmed
  in  case tstr of
    ('.':_) -> read ('0':tstr) 
    _ -> read tstr

convertTraningItem :: RawTrainingItem -> TrainingItem
convertTraningItem rtis = fromList $ P.zipWith (\f i -> f i) (P.take (P.length rtis) $ P.repeat readDouble) rtis

spaceSeparated :: PR.Parser TrainingItem
spaceSeparated = fromList <$> PR.sepBy PR.rational (PR.skipMany $ PR.char ' ')

commaSeparated :: PR.Parser RawTrainingItem
commaSeparated = PR.sepBy (T.pack <$> PR.many1 (PR.satisfy $ PR.inClass "0-9A-Za-z.-")) sep
  where
    sep = do
      PR.skipMany $ PR.char ' '
      PR.char ','
      PR.skipMany $ PR.char ' '

loadDataFromFile :: forall a. FilePath -> PR.Parser a -> IO (Vector a)
loadDataFromFile path parser = (fmap $ toElements).fromList.T.lines.T.pack <$> S.readFile path
  where
    toElements :: T.Text -> a
    toElements line = case (PR.parseOnly parser $ T.strip line) of
      Left err -> error $ "Parse failed : " P.++ err
      Right r  -> r

loadDataFromText :: forall a. T.Text -> PR.Parser a -> (Vector a)
loadDataFromText t parser = toElements <$> (fromList $ T.lines t)
  where
    toElements :: T.Text -> a
    toElements line = case (PR.parseOnly parser $ T.strip line) of
      Left err -> error $ "Parse failed : " P.++ err
      Right r  -> r

looper :: (a -> a) -> a -> Int -> a
looper _ a 0 = a
looper fn a count = looper fn (fn a) (count -1)

looperObserver :: (NFData a) => (a -> a) -> a -> ((Int, a) -> IO ()) -> Int -> IO a
looperObserver fn a ch count = do
  ch (count, a)
  if count > 0 then looperObserver fn (fn a) ch $ deepseq a (count -1) else return a

extractOutputVector :: Int -> TrainingSet -> (TrainingSet, V.Vector P.Double)
extractOutputVector outputIndex ts = let x = (convertTrainingItem <$> ts, (! outputIndex) <$> ts) in x
  where
    convertTrainingItem :: TrainingItem -> TrainingItem
    convertTrainingItem rti = ifilter (\idx a -> idx /= outputIndex) rti

flattenNV :: V.Vector (V.Vector P.Double) -> V.Vector P.Double
flattenNV v = V.foldl (\a v -> a V.++ v) V.empty v
