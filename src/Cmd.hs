module Cmd where

import Lib
import GradientDescent
import NormalEquation
import Data.Decimal
import Debug.Trace
import Data.Monoid
import Data.Vector as V
import Prelude  (putStrLn, Maybe(..), snd, Bool(..), (^), (<), (==), fst, show, pure, read, reads, fromIntegral, error, (-), (/), (*), (+), writeFile, fmap, (.), ($), (<$>), IO, FilePath, Int, String, Either(..), Float)
import qualified Prelude as P
import HMatrixStatic
import qualified Data.Text as T
import GHC.TypeNats
import qualified Numeric.LinearAlgebra as LA
import LogisticRegression
import qualified Data.Map as DM
import Control.Monad.Writer
import Control.Monad.Reader
import Graphs
import Data.Monoid
import NeuralNet
import Data.Aeson as Aeson
import DataGen
import Codec.Picture
import Data.Maybe
import Control.DeepSeq

main :: IO ()
main = do
  ((resultCost, resultTheta), md) <- doMain
  (cost, pairs) <- testTheta (lrHypothesis resultTheta) lrCostFunction md
  P.putStrLn "Result of gradient result"
  P.putStrLn $ "---------------------------"
  P.putStrLn $ ("theta = " P.++ (show resultTheta))
  P.putStrLn $ ("cost = " P.++ (show resultCost))
  P.putStrLn "Test of gradient result"
  P.putStrLn $ "---------------------------"
  P.putStrLn $ ("cost = " P.++ (show cost))
  P.putStrLn $ ("pred/out pairs = " P.++ (show pairs))
  P.return ()

doClassification :: IO ()
doClassification = do
  a <- loadDataFromFile "../Abalone/abalone.data" (P.fmap convertTraningItem commaSeparated) :: IO (Vector TrainingItem)
  let
    (t, o) = extractOutputVector 0 a
    trainingSet = tmatrix $ take 4100 t :: TMatrix 4100 8
    (mnTraningSet, md) = meanNormalize trainingSet :: (TMatrix 4100 8, (TVector 8, TVector 8))
    out = tvector (take 4100 o) :: TVector 4100
    theta = tvector $ fromList $ P.take 9 $ P.repeat 0 :: TVector 9
    classifiers= runOneVsAll theta mnTraningSet out 0.3 5000

    x0 = TMatrix $ LA.fromColumns [LA.vector $ P.take 77 [1,1..]] :: TMatrix 77 1
    testSet = x0 ||| (applyMD (tmatrix $ take 77 $ drop 4000 t) $ md) :: TMatrix 77 9
    testOut :: Vector P.Double = take 77 $ drop 4000 o
    classificationResult = classify classifiers testSet
  P.putStrLn $ show classifiers
  let pairs = P.zip classificationResult (toList testOut)
  P.putStrLn $ show $ pairs
  P.putStrLn $ show $ (P.foldl (\a b -> a + b) 0 $ (\(a, b) -> (a - b)*(a - b)) <$> pairs)/(P.fromIntegral $ P.length pairs)
  P.return ()

doLogisticRegression = do
  t <- loadDataFromFile "../Abalone/abalone.data" (P.fmap convertTraningItem commaSeparated) :: IO (Vector TrainingItem)
  let tsr = take 4100 $ t
  P.putStrLn $ show $ length $ tsr
  let
        (t, o) = extractOutputVector 0 tsr
        trainingSet = tmatrix t :: TMatrix 4100 8
        out = tvector o :: TVector 4100
        theta = tvector $ fromList $ P.take 9 $ P.repeat 1 :: TVector 9
        (e, h, l) = runLogisticRegression theta trainingSet 0.003 1000 out
        in P.putStrLn $ show h
        -- in runOneVsAll theta trainingSet out 0.3 1000

doMain :: IO ((P.Double, TVector 9), (TVector 8, TVector 8))
doMain = do
  a <- loadDataFromFile "../Abalone/abalone.data" (P.fmap convertTraningItem commaSeparated) :: IO (Vector TrainingItem)
  P.putStrLn $ show $ length $ a
  let
      (t, o) = extractOutputVector 8 a
      (trainingSet, md) = meanNormalize $ (tmatrix $ take 4100 t :: TMatrix 4100 8)
      out = tvector (take 4100 o) :: TVector 4100
      theta = tvector $ fromList $ P.take 9 $ P.repeat 0 :: TVector 9
      (result, log) = runWriter $ runGradientDescent trainingSet out lrHypothesis lrCostFunction 0.3 theta 1000 True True
      in return (result, md)

testTheta :: Hypo 77 8 -> CostFunction 77 -> (TVector 8, TVector 8) -> IO (P.Double, [(P.Double, P.Double)])
testTheta hypo costFunction md = do
  t <- loadDataFromFile "../Abalone/abalone.data" (P.fmap convertTraningItem commaSeparated) :: IO (Vector TrainingItem)
  let (tsr, o) = extractOutputVector 8 <$> drop 4100 $ t
  let dataSet = applyMD (tmatrix tsr :: TMatrix 77 8) md
  let (pred, cost) = testHypoOnRaw hypo costFunction dataSet (tvector o)
  P.return (cost, P.zip (LA.toList $ unr pred :: [P.Double]) ((toList o) :: [P.Double]))

normalEquation :: IO ()
normalEquation = do
  tsr <- loadDataFromFile "../Abalone/abalone.data" (P.fmap convertTraningItem commaSeparated) :: IO (Vector TrainingItem)
  let (ts, outs) = extractOutputVector 8 tsr
  let (theta, cost) = runNormalEquationOn (tmatrix $ take 4100 ts :: TMatrix 4100 8) (tvector $ take 4100 outs :: TVector 4100)
  P.putStrLn $ "Normal Equation"
  P.putStrLn $ "---------------------------"
  P.putStrLn $ ("theta = " P.++ (show theta))
  P.putStrLn $ ("cost = " P.++ (show cost))


computeDecision :: Hypothesis -> P.Double -> P.Double -> Bool
computeDecision hypo x y =  let d = (y - (computeHypothesis hypo x)) in d*d < 0.01

computeHypothesis :: Hypothesis -> P.Double -> P.Double
computeHypothesis hypo input = P.foldl (\a term -> a + (coeff term) * (input P.^^ (Lib.exp term))) (0::P.Double) $ terms hypo

testNN :: IO ()
testNN = do
    dg :: DataGenerator 6400 1000 <- getDataGenerator 1
    let 
      nn = getNetwork
    let
      ain = inputs dg
      aout = applyDivider divider $ ain
      pairs = taggedZip (toTaggedRows ain) (toTaggedRows aout)
    newNN <- trainNetwork nn ain aout 0.0003 0 1000 10 $ printCost pairs
    putStrLn $ show $ computeCost newNN pairs 1
    return ()
  where
    printCost pairs (_, Nothing) = return ()
    printCost pairs (c, Just (nn, cost)) = do
      putStrLn $ show cost
