module GradientDescent where

import Lib
import HMatrixStatic
import Data.Vector as V
import Data.Decimal
import Debug.Trace
import qualified Numeric.LinearAlgebra as LA
import Prelude  (Bool, snd, (<), (>), (==), fst, show, pure, read, reads, fromIntegral, error, (-), (/), (*), (+), writeFile, fmap, (.), ($), (<$>), IO, FilePath, Int, String, Either(..), Float)
import qualified Prelude as P
import GHC.TypeNats
import Data.Proxy
import Control.Monad.Writer

type Hypo r c = TMatrix r (1+c) -> TVector r
type ThetaToHypo r c = TVector (1+c) -> Hypo r c
type CostFunction r = (TVector r -> TVector r -> P.Double)

runGradientDescent
  :: forall r c. TMatrix r c
  -> TVector r
  -> ThetaToHypo r c
  -> CostFunction r
  -> P.Double
  -> TVector (1+c)
  -> Int
  -> Bool
  -> Bool
  -> Writer [(TVector (1+c), P.Double)] (P.Double, TVector (1+c))
runGradientDescent trainingSet out hypo costFunction lr theta count doMeanNormalize adjustLr =
  let
      numRows = rows trainingSet
      pred = hypo theta x
      cost = (costFunction pred out)
      x0 = TMatrix $ LA.fromColumns [LA.vector $ P.take numRows [1,1..]] :: TMatrix r 1
      xR = (x0 ||| trainingSet)
      x = xR
      xt = tr xR
      result = runGradientDescent' cost (cost, theta) x xt out (P.realToFrac lr) theta count
    in result
  where
    runGradientDescent'
      :: P.Double
      -> (P.Double, TVector (1+c))
      -> TMatrix r (1+c)
      -> TMatrix (1+c) r
      -> TVector r
      -> P.Double
      -> TVector (1+c)
      -> Int
      -> Writer [(TVector (1+c), P.Double)] (P.Double, TVector (1+c))
    runGradientDescent' _ lowestYet _ _ _ _ _ 0 = return lowestYet
    runGradientDescent' a b c d e 0 f g = return b
    runGradientDescent' lastCost lowestYet titems tiTranspose outs lr c count =  do
      tell [(c, lastCost)]
      let
        newPred = hypo newCoeffs titems
        newCost = costFunction newPred outs
        newCoeffs = oneGradientDescent titems tiTranspose outs lr c
        in traceShow (count, lastCost, newCost, newCost < lastCost, newCost == lastCost, lr) $ if adjustLr then let
          newLowest = if newCost < fst lowestYet then (newCost, newCoeffs) else lowestYet
          in if newCost > fst newLowest
            then runGradientDescent' lastCost lowestYet titems tiTranspose outs (lr/3) c count 
            else runGradientDescent' newCost newLowest titems tiTranspose outs (lr * 5) newCoeffs (count - 1)
        else runGradientDescent' newCost (newCost, newCoeffs) titems tiTranspose outs lr newCoeffs (count - 1)
    oneGradientDescent
      :: TMatrix r (1+c)
      -> TMatrix (1+c) r
      -> TVector r
      -> P.Double
      -> TVector (1+c)
      -> TVector (1+c)
    oneGradientDescent x xt out lr theta = let
      m = (fromIntegral $ rows x )
      predictions = hypo theta x
      errors :: TVector r
      errors = predictions - out
      partialDerivatives :: TVector (1+c)
      partialDerivatives = xt #> errors
      deltas :: TVector (1+c)
      deltas = partialDerivatives * (TVector $ LA.vector [lr/m])
      in theta - deltas

getX0 :: Int -> TMatrix r 1
getX0 r = TMatrix $ LA.fromColumns [LA.vector $ P.take r [1,1..]]

meanNormalize :: forall r c. TMatrix r c -> (TMatrix r c, (TVector c, TVector c))
meanNormalize m = let
  rs = (rows m)
  mt = tr m
  v = (TVector $ LA.vector $ P.take rs [1::P.Double, 1..]) :: TVector r
  avgs = (mt #> v) / (TVector $ LA.vector $ [P.realToFrac rs :: P.Double]) :: TVector c
  mins = TVector $ LA.vector $ fmap LA.minElement $ LA.toRows $ unr mt :: TVector c
  maxs = TVector $ LA.vector $ fmap LA.maxElement $ LA.toRows $ unr mt :: TVector c
  ranges = maxs - mins
  mdata = (avgs, ranges)
  in (applyMD m mdata, mdata)

lrHypothesis :: TVector c -> TMatrix r c -> TVector r
lrHypothesis theta ts = ts #> theta

lrCostFunction :: forall r. TVector r -> TVector r -> P.Double
lrCostFunction pred o =
  let
    numRows = vrows pred
    errors = ((unr (pred - o)) P.^ 2)
    errorSum = (LA.sumElements $ errors)
    divider = (P.realToFrac $ 2 * numRows)
      in errorSum/divider

testHypoOnRaw :: forall r c. Hypo r c -> CostFunction r -> TMatrix r c -> TVector r -> (TVector r, P.Double)
testHypoOnRaw hypothesis costFunction raw out = let
  numRows = (fromIntegral $ rows raw)
  x0 = TMatrix $ LA.fromColumns [LA.vector $ P.take numRows [1,1..]] :: TMatrix r 1
  xR = (x0 ||| raw)
  pred = hypothesis xR
  cost = costFunction  pred out
  in (pred, cost)

applyMD :: forall r c. TMatrix r c -> (TVector c, TVector c) -> TMatrix r c
applyMD ts (avgs, ranges) = let
  cs = cols ts
  rs = rows ts
  avgsMtx = repmat (TMatrix $ LA.fromRows [unr avgs] :: TMatrix 1 c) rs 1
  rangesMtx = repmat (TMatrix $ LA.fromRows [unr ranges] :: TMatrix 1 c) rs 1
  in (ts - avgsMtx)/rangesMtx
