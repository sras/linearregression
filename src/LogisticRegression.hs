module LogisticRegression where

import HMatrixStatic
import GradientDescent
import GHC.TypeNats
import Data.Proxy
import qualified Data.Vector as V
import qualified Numeric.LinearAlgebra as LA
import Debug.Trace
import Data.Map.Strict as DM
import Data.List as DL
import NormalEquation
import Control.Monad.Writer

sigmoid :: Double -> Double
sigmoid x = 1/(1 + (exp $ negate x))

pSigmoid :: Double -> Double
pSigmoid x = let a = (sigmoid x) in a * (1 - a)

classify
  :: forall r c. Map Double (TVector c)
  -> TMatrix r c
  -> [Double]
classify c tin = let
  inputs = TVector <$> (LA.toRows $ unr tin) :: [TVector c]
  in runClassification c <$> inputs
  where
  runClassification 
    :: forall r c. Map Double (TVector c)
    -> TVector c
    -> Double
  runClassification c tin = let m = TMatrix $ LA.fromRows $ [unr tin] :: TMatrix 1 c
    in fst $ DL.maximumBy (\(_, a) (_, b) -> compare a b) $ DM.foldrWithKey (\k v a -> let h = runClassifier v m in (k, h):a) [] c
    where
      runClassifier :: TVector c -> TMatrix 1 c -> Double
      runClassifier theta tin = LA.sumElements $ unr $ tin #> theta

runOneVsAll
  :: forall r c. TVector (1+c)
  -> TMatrix r c
  -> TVector r
  -> Double
  -> Int
  -> Map Double (TVector (1+c))
runOneVsAll start tin tout lr count = Prelude.foldl (\a d -> if DM.member d a then a else let v = getClassifierFor d in DM.insert d v a) DM.empty outList
  where
    outList = LA.toList $ unr tout
    getClassifierFor :: Double -> TVector (1+c)
    getClassifierFor d = let
      ovaOut = tvector $ V.fromList $ (\a -> if a == d then 1 else 0) <$> outList
      (e, t, _) = runLogisticRegression start tin lr count ovaOut
      in t

runLogisticRegression
  :: forall r c. TVector (1+c)
  -> TMatrix r c
  -> Double
  -> Int
  -> TVector r
  -> (Double, TVector (1+c), [(TVector (1 + c), Double)])
runLogisticRegression start tin lr count tout  = let
  ((e, t), log) = runWriter $ runGradientDescent tin tout lgrHypo lgrCostFunction lr start count False True
  in (e, t, log)

lgrHypo :: TVector c -> TMatrix r c -> TVector r
lgrHypo theta tin = let
  a = tin #> theta
  b = cmap sigmoid a
  in b

lgrCostFunction :: forall r. TVector r -> TVector r -> Double
lgrCostFunction pred o = let
  numRows = (fromIntegral $ vrows pred )
  logHTheta :: TVector r = cmap (\x -> if x == 0 then 0 else log x) pred
  logOMHTheta :: TVector r = cmap (\x -> if x == 1 then 0 else log $ 1 - x) pred
  v = unr $ (o*logHTheta) + ((1-o) *  logOMHTheta)
  s = (LA.sumElements $ v)
  in s/(negate numRows)


test :: IO ()
test =
  do
    let
      pred = tvector $ V.fromList [0.8, 0.3, 0.3, 0.6] :: TVector 4
      out = tvector $ V.fromList [1, 0, 0, 1] :: TVector 4
    putStrLn $ show $ lgrCostFunction pred out
