module NormalEquation where

import qualified Numeric.LinearAlgebra as LA
import Lib
import HMatrixStatic
import qualified Data.Vector as V
import Data.Decimal
import Debug.Trace
import GHC.TypeNats
import Prelude as P
import Data.Proxy
import GradientDescent (lrHypothesis, lrCostFunction)

runNormalEquationOnMatrix :: TMatrix r c -> TVector r -> TVector c
runNormalEquationOnMatrix x y = let xt = tr x in (pinv (xt <> x)) #> (xt #> y)

runNormalEquationOn :: forall c r. TMatrix r c -> TVector r -> (TVector (1+c), Double)
runNormalEquationOn x o = let
  numRows = (fromIntegral $ rows x)
  x0 = TMatrix $ LA.fromColumns [LA.vector $ P.take numRows [1,1..]] :: TMatrix r 1
  xR = (x0 ||| x)
  result = runNormalEquationOnMatrix xR o
  in (result, lrCostFunction (lrHypothesis result xR) o)
