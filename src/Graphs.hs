module Graphs where

import HMatrixStatic hiding ((<>))
import Lib
import qualified Numeric.LinearAlgebra as LA
import Debug.Trace
import Data.Vector as V
import Data.Monoid
import Data.Maybe

-- window :: GL.Display
-- window = GL.InWindow "Nice Window" (800, 800) (100, 10)
-- 
-- scatterPlot :: TMatrix a 2 -> (Int, Int, Int) -> Picture
-- scatterPlot ti (r, g, b) =  GL.Color color $ GL.Pictures $ convertToPoint <$> (toRows ti)
--   where
--     color = GL.makeColorI r g b 255
--     convertToPoint :: TVector 2 -> GL.Picture
--     convertToPoint v = let [x, y] = LA.toList $ unr v in GL.Translate (realToFrac x) (realToFrac y) (GL.ThickCircle 2 3)
-- 
-- drawTraningSet :: TrainingSet -> Picture
-- drawTraningSet trainingSet =  let
--     extractData x = V.slice 0 2 x
--     negative = tmatrix $ V.map extractData $ V.filter (\x -> x V.! 2 == 0) trainingSet
--     positive = tmatrix $ V.map extractData $ V.filter (\x -> x V.! 2 == 1) trainingSet
--  in (scatterPlot positive (255,5,5)) <> (scatterPlot negative (5,255,5))
-- 
-- drawDecisionBoundary :: (Int, Int) -> (Int, Int) -> (Double -> Double -> Bool) -> Picture
-- drawDecisionBoundary (startx, endx) (starty, endy) cd = GL.Line $  catMaybes $ do
--   x <- [startx..endx]
--   y <- [starty..endy]
--   let d = cd (realToFrac x) (realToFrac y)
--   return $ if d then Just (realToFrac x, realToFrac y)  else Nothing
-- 
-- displayPicture :: Picture -> IO ()
-- displayPicture p = GL.display window GL.white p
