module HMatrixStatic where

import GHC.TypeLits
import GHC.TypeNats
import qualified Numeric.LinearAlgebra as LA
import Data.Proxy
import Prelude as P
import Prelude ((++))
import qualified Data.Text as T
import Lib
import Data.Vector as V hiding ((++))
import Text.InterpolatedString.Perl6 (qc)
import Debug.Trace
import Control.DeepSeq
import GHC.Generics

newtype TaggedList (n :: Nat) a = TaggedList [a] deriving (Show, Read, Generic, NFData)

instance Functor (TaggedList a) where
  fmap f (TaggedList a) = TaggedList (fmap f a)

data TVector (a :: Nat) = TVector (LA.Vector P.Double) deriving (P.Show, Read, Generic, NFData)
data TMatrix (r :: Nat) (c :: Nat) = TMatrix (LA.Matrix P.Double) deriving (P.Show, Read, Generic, NFData)

instance P.Num (TVector a) where
  (TVector a) + (TVector b) = TVector (a+b)
  (TVector a) * (TVector b) = TVector (a*b)
  abs (TVector a) = TVector $ abs a
  signum (TVector a) = TVector $ signum a
  fromInteger a = TVector $ fromInteger a
  negate (TVector a) = TVector $ negate a

instance P.Num (TMatrix a b) where
  (TMatrix a) + (TMatrix b) = TMatrix (a+b)
  (TMatrix a) * (TMatrix b) = TMatrix (a*b)
  abs (TMatrix a) = TMatrix $ abs a
  signum (TMatrix a) = TMatrix $ signum a
  fromInteger a = TMatrix $ fromInteger a
  negate (TMatrix a) = TMatrix $ negate a

instance P.Fractional (TVector a) where
  fromRational a = TVector $ fromRational a
  recip (TVector a) = TVector $ recip a

instance P.Fractional (TMatrix a b) where
  fromRational a = TMatrix $ fromRational a
  recip (TMatrix a) = TMatrix $ recip a

(#>) :: TMatrix a b -> TVector b -> TVector a
(TMatrix m) #> (TVector v) = TVector (m LA.#> v)

(<>) :: TMatrix a b -> TMatrix b c -> TMatrix a c
(TMatrix m1) <> (TMatrix m2) = TMatrix (m1 LA.<> m2)

(|||) :: TMatrix a b -> TMatrix a c -> TMatrix a (b+c)
(TMatrix m1) ||| (TMatrix m2) = TMatrix (m1 LA.||| m2)

tr :: TMatrix a b -> TMatrix b a
tr (TMatrix m1) = TMatrix $ LA.tr m1

pinv :: TMatrix a b -> TMatrix a b
pinv m = TMatrix $ LA.pinv $ unr m

toTaggedRows :: TMatrix a b -> TaggedList a (TVector b)
toTaggedRows m = TaggedList $ TVector <$> (LA.toRows $ unr m)

cmap :: (Double -> Double) -> TVector a -> TVector a
cmap fn m= TVector $ LA.cmap fn $ unr m

class Unwrappable a b | a -> b where
  unr :: a -> b

instance Unwrappable (LA.Vector Double) [Double] where
  unr a = LA.toList a

instance Unwrappable (TVector a) (LA.Vector P.Double) where
  unr (TVector a) = a

instance Unwrappable (TMatrix a b) (LA.Matrix P.Double) where
  unr (TMatrix a) = a

instance Unwrappable (TaggedList a b) [b] where
  unr (TaggedList a) = a

fromVector :: TVector a -> TMatrix a 1
fromVector (TVector v) = TMatrix $ LA.fromColumns [v]

rows :: TMatrix a b -> Int
rows (TMatrix x) = LA.rows x

cols :: TMatrix a b -> Int
cols (TMatrix x) = LA.cols x

vrows :: TVector b -> Int
vrows (TVector x) = LA.size x

toRows :: TMatrix r c -> [TVector c]
toRows m = TVector <$> (LA.toRows $ unr m)

repmat :: forall r c x y. TMatrix r c -> Int -> Int -> TMatrix x y
repmat (TMatrix m) rowMul colMul = TMatrix $ LA.repmat m rowMul colMul

tmatrixSafe :: forall r c. TrainingSet -> Either String (TMatrix r c)
tmatrixSafe ts =
  let
    reqRow = V.length ts
    reqCol = V.length $ V.head ts
    rows = V.length ts
    cols = V.length $ V.head ts
    flat = flattenNV $ ts
    dimOk = V.length flat == reqRow * reqCol
    in if dimOk then Right (TMatrix $ reqRow LA.>< reqCol $ toList $ flat)
      else let
        offendingRows = V.filter (\x -> V.length x /= reqCol) ts
        in Left $ [qc|Data does not match dimensions to create matrix, required  {reqRow} X {reqCol}. Given, {rows} X {cols}, (Have {V.length flat}  but require  {reqRow * reqCol}. Offending rows {offendingRows}) |]


tmatrixZeros :: Int -> Int -> TMatrix r c
tmatrixZeros r c = (tmatrix $ V.fromList $ P.take r $ P.repeat $ getZeros c)
  where
    getZeros :: Int -> Vector Double
    getZeros c = V.fromList $ P.take c $ repeat 0

tmatrix :: forall r c. TrainingSet -> TMatrix r c
tmatrix ts = case tmatrixSafe ts of
  Right x -> x
  Left e -> error e

dropColumn :: TMatrix r c -> TMatrix r (c-1)
dropColumn (TMatrix m) = TMatrix $ LA.dropColumns 1 m

dropZero :: TVector r -> TVector (r-1)
dropZero (TVector v) = TVector $ LA.subVector 1 (LA.size v - 1) v

tvector :: Vector P.Double -> TVector a
tvector o =
  let
    s = V.length o
    in TVector $ LA.vector $ toList $ o

taggedZip :: TaggedList a b -> TaggedList a c -> TaggedList a (b,c)
taggedZip (TaggedList a) (TaggedList b) = TaggedList $ P.zip a b

taggedZipWith :: forall a b c h . (a -> b -> c) -> TaggedList h a -> TaggedList h b -> TaggedList h c
taggedZipWith fn (TaggedList a) (TaggedList b) = TaggedList $ P.zipWith fn a b

taggedTail :: TaggedList a h -> TaggedList (a-1) h
taggedTail (TaggedList a) = TaggedList $ P.tail a

taggedInit :: TaggedList a h -> TaggedList (a-1) h
taggedInit (TaggedList a) = TaggedList $ P.init a

taggedLast :: TaggedList a h -> h
taggedLast (TaggedList a) = P.last a

tvectorSafe :: Vector P.Double -> TVector a
tvectorSafe o =
  let
    s = V.length o
    in TVector $ LA.vector $ toList $ o

showVector :: LA.Vector Double -> T.Text
showVector v = T.dropAround (\c -> c == '[' || c == ']') $ T.pack $ show v

showVectorVertical :: T.Text -> T.Text
showVectorVertical t = T.intercalate "\n" $ (T.split (\x -> x == ',') t)

showMatrix :: LA.Matrix Double -> T.Text
showMatrix m = T.unlines $ showVector <$> LA.toRows m

showTMatrix :: TMatrix a b -> T.Text
showTMatrix (TMatrix m) = showMatrix m

vcons :: Double -> TVector a -> TVector (1+a)
vcons a v = TVector $ LA.vjoin [(LA.fromList [a]), unr v]

vtails :: TVector (1+a) -> TVector a
vtails v = let u = unr v in TVector $ LA.subVector 1 (LA.size u - 1) u
